console.log('Hello World!');

/* 
	[[ FOR REVISION ]]
	TASK 1: Create a function that will prompt the user for 2 numbers and perform different arithmetic operations based on the total of the two numbers:
		a. If the total of the two numbers is less than 10, add the numbers
		b. If the total of the two numbers is 10 - 20, subtract the numbers
		c. If the total of the two numbers is 21 - 29 multiply the numbers
		d. If the total of the two numbers is greater than or equal to 30, divide the numbers
		e. Use an alert for the total of 10 or greater and a console warning for the total of 9 or less.
*/
function arithmeticOperations(){
	// input numbers
	let num1 = parseInt(prompt('Please enter first number: '));
	let num2 = parseInt(prompt('Please enter second number: '));

	// total of 2 numbers
	let totalNum = num1 + num2;

	if (totalNum < 10) {
		totalNum = num1 + num2; // 1+1
		console.log('The total of two numbers is: ' + totalNum);
	} else if (totalNum >= 10 && totalNum <= 20) {
		totalNum = num1 - num2; // 15 + 5
		alert('The difference of two numbers is: ' + totalNum);
	} else if (totalNum >= 21 && totalNum <= 29) { 
		totalNum = num1 * num2; // 13 + 8
		alert('The product of two numbers is: ' + totalNum);
	} else if (totalNum >= 30) { // 250 + 5
		totalNum = num1 / num2;
		alert('The quotient of two numbers is: ' + totalNum);
	} else {
		console.log('The total of two numbers is: ' + totalNum);
	}

}

/*
	2. Create a function that will prompt the user for their name and age and print out different alert messages based on the user input:
		-> If the name OR age is blank/null, print the message are you a time traveler?
		
		-> If the name AND age is not blank, print the message with the user’s name and age.
*/
function userDetails() {
	let name = prompt('What is your name?');
	let age = parseInt(prompt('Please enter your age:'));

	return (name && age !== null) ? alert('Hi, ' + name + '. Your age is: ' + age + '!') : alert('Are you a time traveler?');
}



/*
	TASK 3: Create a function with switch case statement that will check if the user's age input is within a certain set of expected input:
	- 18 - print the message You are now allowed to party.
	- 21 - print the message You are now part of the adult society.
	- 65 - print the message We thank you for your contribution to society.
	- Any other value - print the message Are you sure you're not an alien?
	
*/
function ageInput() {
	let age = parseInt(prompt('Please enter your age: '));
	let response;

	switch (age) {
		case 18:
			response = "You are allowed to party!";
				return alert(response);
				break;
		case 21:
			response = "You are now part of the adult society!";
				return alert(response);
				break;
		case 65:
			response = "We thank you for your contribution to the society!";
				return alert(response);
				break;
		default:
				return alert("Are you sure you're not an alien?");
				break;
	}
}