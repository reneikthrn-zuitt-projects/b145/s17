// Test
console.log('Hello World!');

// CONTROL STRUCTURES
// Section 1.] if-else Statement

let numA = 3;

/* 
	Sub Section 1.1] if statement
		- The task of the if statement is to execute a procedure/action if the specified condition is "true".
*/
if (numA <=0) {
	// Truthy Branch
	// This block of code will run if the condition is MET.
	console.log('The condition was MET!');
}

/*
	let name = 'Lorenzo'; // Allowed
	Maria // Not Allowed
*/

let isLegal = false;
// ! -> NOT = FALSE

// Create a control structure that will allow the user to proceed if the value matches/passes the condition.
if (!isLegal) {
	// If it passes the condition, the "truthy" branch will run
	console.log('User can proceed!');
}


/* 
	Sub Section 1.2] else statement
		- This executed a statement if all other conditions are "FALSE" and/or has failed.
*/

/* 
	let's create a control structure that will allow us to simulate a user login.

	PROMPT BOX => prompt(): this will allow us to display a prompt box which we can have the user enter an input.
		Syntax: 
			prompt(param1 *required, param2 *optional); -> proompt (text/message, placeholder);
*/

// prompt("Please enter your first name: ", "Renei");

// let's create a function that will allow us to checkc if the user is old enough to drink
/*
	let age = 21;

	if (age >= 18) {
		// 'Truthy' Branch
		alert("You're old enough to drink!");
	} else {
		// 'Falsy' Branch
		alert("Come back another day.");
	}
*/

// let's create a control structure that will ask for the number of drinks that you will order

// Task 1: Ask the user how many drinks he wants
//let order = prompt('How many order of drinks do you like?');
//console.log("Original value of prompt: " + typeof order); // Originally: STRING

/*
	Conversion: Convert the string data type into a number.
		parseInt() => will allow us to convert strings into integers
*/
// order = parseInt(order); // Now converted to a NUMBER
//console.log("Value of prompt upon conversion (repeat method): "+ order);

/*
	3 WAYS TO MULTIPLY A STRING:
		1. repeat() method 
			- this will allow us to return a new string value that contains the number of copies of the string
				Syntax: <str>.repeat(number/value);

		2. loops => for loop
		3. loops => while loop method
*/

// Task 2: Create a logic that will make sure that the user's input is greater than 0
/*
if (order > 0) {
	let cocktail = '🍸'; // STRING
	alert(cocktail.repeat(order)); // Error: NaN
} else {
	alert('The number should be greater than 0!');
}
*/

	/* 
		TYPE COERCION => Conversion data type was convverted to another data type

		> COMPOSITE - stores multiple data
		> PRIMITIVE - stores single data

			1.) If one of the operands is an object, it will be converted into a primitive data type such as; str, number, boolean
			2.) If atleast 1 operand is a string, it will convert the other operand into a string as well
			3.) If both numbers are numbers then an arithmetic operation will be executed
	*/


// -- MINI-ACTIVITY -- formulate a logic that will allow us to multiply a string to a desired number


// Mini Task #1: Vaccine Checker
function vaccineChecker(){
	let vax = prompt('What brand is your vaccine?'); //Ask input from the user

	  /*
	  	pfizer, PFIZER
	  		- We need to process the input of the user - so that whenever input the user will enter, we can control the uniformity of the strings.
	  
	  		* toLowerCase() 
	  			-  this will allow us to convert a string into all lowercase characters. 
	  		
	  		Syntax: string.toLowerCase()
	  */

	vax = vax.toLowerCase();
	  
	//create a logic that will allow us to check the values inserted by the user to match the given parameters.
	if (vax === 'pfizer' || vax ===  'moderna' ||vax === 'astrazenica' || vax === 'janssen'){
		//display the response back to the client.
		alert(vax + ' is allowed to travel'); 
	} else {
		alert('Sorry you are not permitted to travel.');
	}

}

/*
	for this mini task we want the user to be able to invoke this function with the user of a trigger.
	vaccineChecker(); // navigate to index.html -> onclick="vaccineChecker()"

	HOW TO ACCESS JS FUNCTION ON HTML FILE?
	* onclick
		- is an example of a JS Event. this "event" in JS occurs when the user clicks on an element/component to trigger a certain procedure. 

	Syntax: <element/component onclick="myScript/functionName()"> 
*/


// Mini Task #2: Typhoon Checker
function determineTyphoonIntensity(){
	// Ask input from the user
	// We will be needing a 'NUMBER' data type so that we can properly compare the values

   	let windspeed = parseInt(prompt('Wind speed:'));
   	console.log(typeof windspeed); //this is to prove that we can directly pass the value of the variable and feed to the the parseInt method.
   
   	//for this logic/structure .. it will only assess the whole number.

   	//note: "else if" is 2 words
  	if(windspeed <= 29) {
   		alert('Not a Typhoon Yet!'); 
   	} else if(windspeed <= 65) {
      	//this will run if the 2nd statement was met.
      	alert('Tropical Depression Detected'); 
   	} else if(windspeed <= 88) {
      	alert('Tropical Storm Detected');
   	} else if(windspeed <= 117) {
      	alert('Severe Tropical Storm');
   	} else {
     	alert('Typhoon Detected'); 
   }
   
}


/*
	CONDITIONAL TERNARY OPERATOR
		- still follows the same syntax with an if-else (truthy, falsy)
		- the only JS Operator that takes 3 OPERANDS such as:
			? Question Mark - describe a condition that if resulted to "true" will run "truthy".
			: Colon - This symbol will separate the truthy and faly statements

	Syntax:
		condition ? "truthy" : "falsy"
*/

// Mini Task #3: Age Checker
function ageChecker() {

  	let age = parseInt(prompt('How old are you?'));
  	// let's simplify our structure below with the help of a ternary operator
 
  	//ternary structure is a short hand version of if else.
  	return (age >= 18) ? alert('You are eligible to vote!') : alert('Not yet eligible to vote.');
  	
  	/*
	   if (age >= 18) {
	    //truthy
	     alert('Old enough to vote');
	   } else {
	     //falsy
	     alert('Not yet old enough');
	   }
   */
}


// Mini Task #4: Function that will determine the owner of a computer unit
// Switch Statement
function determineComputerOwner() {

	let unit = parseInt(prompt('what is the unit no. ?'));
	let manggagamit;

	switch (unit) {
		case 1:
			manggagamit = "John Travolta";
				return alert(manggagamit);
				break;
		case 2:
			manggagamit = "Steve Jobs";
				return alert(manggagamit);
				break;
		case 3:
			manggagamit = "Sid Meier";
				return alert(manggagamit);
				break;
		case 4:
			manggagamit = "Onel de guzman";
				return alert(manggagamit);
				break;
		case 5:
			manggagamit = "Christian salvador";
				return alert(manggagamit);
				break;			
		default:
				return alert('Not here');
				break;
	}

}
// determineComputerOwner();
//passed down determineComputerOwner(); into its button component in the page.


/*
	WHEN TO USE "" (Double Quotes) over '' (Single Quotes)?
	-> "name" === 'name'
	
	we can use either methods to declare a string value. however we can use one over the other to ESCAPE the scope of the initial symbol. 

*/

let dialog = "'Drink your water bhie' - mimiyuhh says";
let dialog2 = ' "I shall return" - McArthur '; 
let ownership = ' Aling Nena\'s Tindahan '//alternative in inserting apostrophe.
console.log(ownership); 



// Activity: Create a demo video on the 4 functions that we have created.
// Deadline: Monday (5:45 pm)
